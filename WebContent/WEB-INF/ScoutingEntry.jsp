<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Main Page</title>
<link rel="stylesheet" type="text/css" href="Style.css">
<style>
input[type=number]{
    width: 80px;
} 
</style>
</head>
<body>
<img src="${pageContext.request.contextPath}/images/pen.jpg" height = "500px" width = "1000px" align = "middle"/>
<a name="home"></a>
<div>
<h1>Welcome to 3075's Website!!!!!</h1>
<ul>
  <li><a href="index.jsp">Home</a></li>
  <c:if test = "${loggedIn == '1'}">
  	<li><a href = "userInfo.jsp">User Info</a></li>
  </c:if>
    <li><a href="about.jsp">About</a></li>  
  <li><a href="ContactUs.jsp">Contact</a></li>
  <c:if test = "${loggedIn == NULL}">
  	<li><a href="Signup.jsp">Sign Up</a></li>
  </c:if>
  <c:if test = "${isAdmin == '12345'}">
  	<li><a href="usersTable.jsp">Users Table</a></li>
  
  </c:if>
  <c:if test = "${loggedIn == '1'}">
  	<li><a href = "logout.jsp">Logout</a></li>
  </c:if>
</ul>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
url="jdbc:mysql://localhost/userdb"
user="root"  password="1234"/>
<sql:query dataSource="${snapshot}" var="result">
select * from members where user ='<%=session.getAttribute("user")%>';
</sql:query>
<%
if(application.getAttribute("counter") == null || (Integer)application.getAttribute("counter") == 0) {
    application.setAttribute("counter", 1);
}
if(session.isNew() == true){
    synchronized(page){
    Integer counter = (Integer) application.getAttribute("counter");
    counter += 1;
    application.setAttribute("counter", counter);
}
}
%>
<h2>You are the visitor number <%=application.getAttribute("counter")%></h2>
</br>
<c:if test = "${loggedIn == NULL}">
<hr>
<form id="login" action="LoginCheck.jsp" method="get">
<h2><a href = "Signup.jsp">Click here to signup</a></h2>
<hr>
</c:if>
<c:if test = "${loggedIn == '1'}">
<hr>
</br>

<h1>Successfully logged in as <%=session.getAttribute("user")%>!!!</h1>

<table>
<tr>
<form id = "scoutingOne" action="ScoutingGame" method="get">
<th> Scouting </th>
<td> Game number: <input type="number" name="game" placeholder="0" > </td>
<td> Team number: <input type="number" name="team" placeholder="3075" > </td>
<h2>Match Type: </h2><br>
<input type="radio" name="gametype" value="Training"> Training Match<br>
<input type="radio" name="gametype" value="Qualifier"> Qualifier Match<br>
<input type="radio" name="gametype" value="Final"> Finals Match<br>
</tr>
</table>
<br>
<input type="submit">
<input type="reset">
</form>
</div>
</c:if>
</body>
</html>