<%@ page language="java" contentType="text/html; charset=windows-1255" pageEncoding="windows-1255"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1255">
<title>Main Page</title>
<link rel="stylesheet" type="text/css" href="Style.css">
<style>
input[type=number]{
    width: 80px;
} 
</style>
</head>
<body>
<img src="${pageContext.request.contextPath}/images/pen.jpg" height = "500px" width = "1000px" align = "middle"/>
<a name="home"></a>
<div>
<h1>Welcome to 3075's Website!!!!!</h1>
<ul>
  <li><a href="index.jsp">Home</a></li>
  <c:if test = "${loggedIn == '1'}">
  	<li><a href = "userInfo.jsp">User Info</a></li>
  </c:if>
    <li><a href="about.jsp">About</a></li>  
  <li><a href="ContactUs.jsp">Contact</a></li>
  <c:if test = "${loggedIn == NULL}">
  	<li><a href="Signup.jsp">Sign Up</a></li>
  </c:if>
  <c:if test = "${isAdmin == '12345'}">
  	<li><a href="usersTable.jsp">Users Table</a></li>
  
  </c:if>
  <c:if test = "${loggedIn == '1'}">
  	<li><a href = "logout.jsp">Logout</a></li>
  </c:if>
</ul>
<sql:setDataSource var="snapshot" driver="com.mysql.jdbc.Driver"
url="jdbc:mysql://localhost/userdb"
user="root"  password="1234"/>
<sql:query dataSource="${snapshot}" var="result">
select * from members where user ='<%=session.getAttribute("user")%>';
</sql:query>
<%
if(application.getAttribute("counter") == null || (Integer)application.getAttribute("counter") == 0) {
    application.setAttribute("counter", 1);
}
if(session.isNew() == true){
    synchronized(page){
    Integer counter = (Integer) application.getAttribute("counter");
    counter += 1;
    application.setAttribute("counter", counter);
}
}
%>
<h2>You are the visitor number <%=application.getAttribute("counter")%></h2>
</br>
<c:if test = "${loggedIn == NULL}">
<hr>
<form id="login" action="LoginCheck.jsp" method="get">
<h2><a href = "Signup.jsp">Click here to signup</a></h2>
<hr>
</c:if>
<c:if test = "${loggedIn == '1'}">
<hr>
</br>

<h1>Successfully logged in as <%=session.getAttribute("user")%>!!!</h1>

<table>
<tr>
<form id = "scoutingTwo" action="DataEntry.jsp" method="get">
<th> Game </th>

<h2>Operated </h2><br>
<input type="radio" name="Operation" value="All"> All game<br>
<input type="radio" name="Operation" value="Stopped"> Stopped mid game<br>
<input type="radio" name="Operation" value="None"> Never operated<br>
<input type="radio" name="Operation" value="NoShow"> Never showed up<br>
<br>
<h2>Autonomous</h2>
<input type="checkbox" name="Auto" value="NoMove">Never moved<br>
<input type="checkbox" name="Auto" value="Cross">Crossed the line<br>
<input type="checkbox" name="Auto" value="ScoreLow">Scored to the low post<br> 
<input type="checkbox" name="Auto" value="ScoreHigh">Scored to the high post<br> 
<input type="checkbox" name="Auto" value="ScoreGear">Scored a gear<br>

<h2>Ball Pickup Mechanic</h2>
<input type="checkbox" name="PickupBall" value="NoMove">Floor<br>
<input type="checkbox" name="PickupBall" value="Cross">Human player<br>
<input type="checkbox" name="PickupBall" value="ScoreLow">Hoppers<br> 
<input type="checkbox" name="PickupBall" value="ScoreHigh">None<br> 

<h2>Shooting Speed</h2><br>
<input type="radio" name="ShotSpeed" value="0"> 0 &nbsp;
<input type="radio" name="ShotSpeed" value="1"> 1 &nbsp;
<input type="radio" name="ShotSpeed" value="2"> 2 &nbsp;
<input type="radio" name="ShotSpeed" value="3"> 3 &nbsp;
<input type="radio" name="ShotSpeed" value="4"> 4 &nbsp;
<input type="radio" name="ShotSpeed" value="5"> 5 &nbsp;
<br>

<h2>Shooting Tries</h2><br>
<input type="radio" name="ShotTry" value="0"> 0 &nbsp;
<input type="radio" name="ShotTry" value="1"> 1 &nbsp;
<input type="radio" name="ShotTry" value="2"> 2 &nbsp;
<input type="radio" name="ShotTry" value="3"> 3 &nbsp;
<input type="radio" name="ShotTry" value="4"> 4 &nbsp;
<input type="radio" name="ShotTry" value="5"> 5 &nbsp;
<input type="radio" name="ShotTry" value="6"> 6 &nbsp;
<input type="radio" name="ShotTry" value="7"> 7 &nbsp;
<input type="radio" name="ShotTry" value="8"> 8 &nbsp;
<input type="radio" name="ShotTry" value="9"> 9 &nbsp;
<input type="radio" name="ShotTry" value="10"> 10 &nbsp;
<br>
<br>

<h2>Shooting Spot</h2>
<input type="checkbox" name="ShotSpot" value="None">None<br>
<input type="checkbox" name="ShotSpot" value="Wall">Close to wall<br>
<input type="checkbox" name="ShotSpot" value="HopperClose">Close hopper<br> 
<input type="checkbox" name="ShotSpot" value="HopperFar">Far hopper<br> 
<input type="checkbox" name="ShotSpot" value="Gears">Gears<br>
<input type="checkbox" name="ShotSpot" value="Other">Other<br>


<h2>Gears Pickup Mechanic</h2>
<input type="checkbox" name="Pickup" value="NoMove">Floor<br>
<input type="checkbox" name="Pickup" value="Cross">Human player<br>
<input type="checkbox" name="Pickup" value="ScoreHigh">None<br> 

<h2>Gears Scored</h2><br>
<input type="radio" name="Gears" value="0"> 0 &nbsp;
<input type="radio" name="Gears" value="1"> 1 &nbsp;
<input type="radio" name="Gears" value="2"> 2 &nbsp;
<input type="radio" name="Gears" value="3"> 3 &nbsp;
<input type="radio" name="Gears" value="4"> 4 &nbsp;
<input type="radio" name="Gears" value="5"> 5 &nbsp;
<input type="radio" name="Gears" value="6"> 6 &nbsp;
<input type="radio" name="Gears" value="7"> 7 &nbsp;
<input type="radio" name="Gears" value="8"> 8 &nbsp;
<input type="radio" name="Gears" value="9"> 9 &nbsp;
<input type="radio" name="Gears" value="10"> 10 &nbsp;
<input type="radio" name="Gears" value="11"> 11 &nbsp;
<input type="radio" name="Gears" value="12"> 12 &nbsp;
<input type="radio" name="Gears" value="13"> 13 &nbsp;
<br>
<br>

<h2>Driver Ability</h2><br>
<input type="radio" name="Driver" value="0"> 0 &nbsp;
<input type="radio" name="Driver" value="1"> 1 &nbsp;
<input type="radio" name="Driver" value="2"> 2 &nbsp;
<input type="radio" name="Driver" value="3"> 3 &nbsp;
<input type="radio" name="Driver" value="4"> 4 &nbsp;
<input type="radio" name="Driver" value="5"> 5 &nbsp;
<br>
<br>

<h2>Robot speed</h2><br>
<input type="radio" name="RoboSpeed" value="0"> 0 &nbsp;
<input type="radio" name="RoboSpeed" value="1"> 1 &nbsp;
<input type="radio" name="RoboSpeed" value="2"> 2 &nbsp;
<input type="radio" name="RoboSpeed" value="3"> 3 &nbsp;
<input type="radio" name="RoboSpeed" value="4"> 4 &nbsp;
<input type="radio" name="RoboSpeed" value="5"> 5 &nbsp;
<br>
<br>

<h2>Robot Stability</h2><br>
<input type="radio" name="RoboStable" value="0"> 0 &nbsp;
<input type="radio" name="RoboStable" value="1"> 1 &nbsp;
<input type="radio" name="RoboStable" value="2"> 2 &nbsp;
<input type="radio" name="RoboStable" value="3"> 3 &nbsp;
<input type="radio" name="RoboStable" value="4"> 4 &nbsp;
<input type="radio" name="RoboStable" value="5"> 5 &nbsp;
<br>
<br>

<h2>Teamwork</h2><br>
<input type="radio" name="Teamwork" value="0"> 0 &nbsp;
<input type="radio" name="Teamwork" value="1"> 1 &nbsp;
<input type="radio" name="Teamwork" value="2"> 2 &nbsp;
<input type="radio" name="Teamwork" value="3"> 3 &nbsp;
<input type="radio" name="Teamwork" value="4"> 4 &nbsp;
<input type="radio" name="Teamwork" value="5"> 5 &nbsp;
<br>
<br>


<h2>Strategy </h2><br>
<input type="radio" name="Operation" value="Attack"> Attack<br>
<input type="radio" name="Operation" value="Defense"> Defense<br>
<input type="radio" name="Operation" value="Both"> Both<br>
<input type="radio" name="Operation" value="None"> No Strategy<br>
<br>

<h2>Defense Quality (If relevant)</h2><br>
<input type="radio" name="RoboDefense" value="1"> 1 &nbsp;
<input type="radio" name="RoboDefense" value="2"> 2 &nbsp;
<input type="radio" name="RoboDefense" value="3"> 3 &nbsp;
<input type="radio" name="RoboDefense" value="4"> 4 &nbsp;
<input type="radio" name="RoboDefense" value="5"> 5 &nbsp;
<br>
<br>
 
 <td> <h2>Notes: </h2><br>
 <input type="text" name="Notes" placeholder="Your answer" > </td>
</tr>
</table>
<br>
<input type="submit">
<input type="reset">
</form>
</div>
</c:if>
</body>
</html>